# Full-Text Search 

## Abstract:
---
This paper talks about Full-Text Search which lets us look for phrases in textual content data. Here we have briefly seen how Full-text search works and what are challenges associated with it. Moreover, we have explored Elasticsearch, Solr, and Lucene Text Search methods to see their features and how they can be implemented for getting better and faster search results. 

---
## Table of Contents :
---

  1. [Introduction](#introduction)

  2. [Working of Full-Text Search and associated issues](#working-of-full-text-search-and-associated-issues)

  3. [Full-Text Search examples to handle these challenges](#full-text-search-examples-to-handle-these-challenges)
      
        - [Elasticsearch](#elasticsearch) 

        - [Solr](#2solr)

        - [Lucene](#3lucene)

  4. [Applications of Full Text Search](#applications-of-full-text-search) 

  5. [Conclusion](#conclusion)

  5. [References](#references)

 ---
  ## 1. Introduction 
 ---

- With the rapid development of the Internet and with the explosive growth of Web information, Internet users how to remove the impurities and retained the essence quickly and easily to gain the information they need in the vast ocean of information to become a hot research topic in this field. Here Full-Text Search comes as a savior.

- Full-Text Search refers to searching some text inside extensive text data stored electronically and returning results that contain some or all of the words from the query. For example, a search engine will use a full-text search to look for keywords in all the web pages that it indexed. The key to this technique is indexing.
	
- Traditionally text search would return exact matches. In Full-Text Search, the query is usually called a token. Index and token can be used for any matching documents. Various techniques can then be used to extract the data.

 ---
 ## 2. Full-Text Search and associated issues       
 ---
 ###	2.1. Working:
 
 - Build a Text Database

	Firstly, we should build a text database that is used to store all information retrieved by the user, then determine the text model of the retrieval system.

 - Creating Index

	Create an index with the model according to the text of the database. Indexing can greatly improve the speed of information retrieval. Which way you use depends on the scale of the information retrieval system.

 - Search

	After indexing the documents, you can start to search information you need. Search requests are submitted by the users and information retrieval systems preprocess and search the information eventually return user the information.

 - Filter and Sort the Results

	After the information retrieval system searches for the information that the users need and it will filter or sort the information by making a certain rule and then return the user-related information.

###	2.2. Considerations before a full-text search:

 - Necessary features:
		
	Adding a full-text index to the database will help optimize text search. Still, it might need additional features such as auto-complete suggestions, synonym search, or custom scoring for relevant results.

 - Architectural complexity: 
		
	Adding additional software to the architecture means separate systems to maintain and additional software complexity to query two different sources.

 - Costs:

	Whether a solution is built in-house or uses a third-party tool, additional charges are to be expected.

###	2.3. Challenges:

- Scale - Working with huge amounts of data.
- Proximity - Making the search results relevant.
- Handling Synonyms, Abbreviations, homonyms, Phonetics
- Handling misspellings
- Searching through images
- Re-indexing - Handling new or constantly changing data. 

---
## 3. Full-Text Search Examples to handle these challenges
---
### 3.1. Elasticsearch: 
---

- Overview:

	Elasticsearch is a search engine based on the Lucene library. It provides a distributed, multitenant-capable full-text search engine with an HTTP web interface. Elasticsearch is developed in Java and it centrally stores the data for lightning-fast search, fine‑tuned relevancy, and powerful analytics that scale with ease. It can perform a search with all data types - Numbers, text, geo, structured and unstructured.

- Features:

	1. Elasticsearch can be used to search any kind of document. It provides scalable search, has near real-time search, and supports multitenancy. 

	2. Elastic search is distributed, which means that indices can be divided into shards and each shard can have zero or more replicas. Elasticsearch uses Lucene and tries to make all its features available through the JSON and Java API. 

	3. It supports facetting and percolating (a form of prospective search) which can be useful for notifying if new documents match registered queries. Another feature, "gateway", handles the long-term persistence of the index, for example, an index can be recovered from the gateway in the event of a server crash. 

---
### 3.2. Solr 
---

- Overview: 

	Solr is an open-source enterprise-search platform, written in Java. Solr runs as a standalone full-text search server. It uses the Lucene Java search library at its core for full-text indexing and search and has REST-like HTTP/XML and JSON APIs that make it usable from most popular programming languages.

- Features:

	1. Its major features include full-text search, hit highlighting, faceted search, real-time indexing, dynamic clustering, database integration, NoSQL features, and rich document (e.g., Word, PDF) handling. 
	
	2. It is highly reliable, scalable, and fault-tolerant, providing distributed indexing, replication, load-balanced querying, automated failover and recovery, centralized configuration, and more. Solr powers the search and navigation features of many of the world's largest internet sites.
---
### 3.3. Lucene
---
- Overview: 

	Lucene is a free and open-source search engine software library supported by the Apache Software Foundation and is widely used as a standard foundation for non-research search applications. Lucene Core is a Java library providing powerful indexing and search features, as well as spellchecking, hit highlighting, and advanced analysis/tokenization capabilities.

- Features:

	1. While suitable for any application that requires full-text indexing and searching capability, Lucene is recognized for its utility in the implementation of Internet search engines and local, single-site searching. 

	2. Lucene has also been used to implement recommendation systems. For example, Lucene's 'MoreLikeThis' Class can generate recommendations for similar documents. 

---
## 4. Applications of Full-Text Search
---

- Web Search on various websites

- Data Mining

- Banking and Share Markets

- Criminal Investigations

- Scientific Research Methodologies and more.

---
 ## 5. Conclusion
---
    
This paper briefly explains the working of Full-Text Search and analyzes three common but powerful Full-Text Search methods. Based on different parameters like the size of the database, speed of searching and filtering, accuracy, and relevant result, the method should be chosen to suit the need of the user/organization.

---
## 6. References :
---

- [Wikipedia](https://en.wikipedia.org/wiki/Full-text_search) 

- [MongoDB](https://www.mongodb.com/basics/full-text-search#:~:text=Full%2Dtext%20search%20refers%20to,search%20would%20return%20exact%20matches)

- [Solr](https://solr.apache.org/)

- [ElasticSearch](https://www.elastic.co/elasticsearch/)
